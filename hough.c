/***************************************************
*           Bibliotecas         *
***************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <time.h>
#include <pmmintrin.h>
#include <math.h>
#define PI 3.14159265358979323846


/***************************************************
*           Funciones Lab           *
***************************************************/

FILE *abrir_archivo(char nombre_archivo[], char modo[]){
	FILE *archivo;
	if(!(archivo = fopen(nombre_archivo, modo))){
		printf("\nError a abrir el archivo, archivo no encontrado o nombre escrito incorrectamente");
		exit(1);
	}
	return archivo;
}


/***************************************************
*           Función Main            *
***************************************************/


int main(int argc, char* argv[]) {
////////////////////////    Variables Iniciales     ////////////////////////
    char * nombre_archivo = NULL;
    char * nombre_salida = NULL; 
    int c;
    int numero_filas;
    int numero_columnas;
    int numero_angulos;
    int numero_distancias;
    float umbral;
    FILE *archivo_salida;
    

    opterr = 0;



////////////////////////    Validamos Entradas        ////////////////////////
    while ((c = getopt(argc, argv, "i:o:M:N:T:R:U:")) != -1) switch (c) {
        case 'i':
            nombre_archivo = optarg;
            break;
        case 'o':
            nombre_salida = optarg;
            break;
        case 'M':
            sscanf(optarg, "%d", &numero_filas);
            break;
        case 'N':
            sscanf(optarg, "%d", &numero_columnas);
            break;
        case 'T':
            sscanf(optarg, "%d", &numero_angulos);
            break;
        case 'R':
            sscanf(optarg, "%d", &numero_distancias);
            break;
        case 'U':
            sscanf(optarg, "%f", &umbral);
            break;
        case '?':
        if (optopt == 'i' || optopt == 'o' ||   optopt == 'M'  ||  optopt == 'N'||  optopt == 'T' || optopt == 'R' ||  optopt == 'U'){
            fprintf (stderr, "Opcion -%c requiere un argumento.\n", optopt);
            return 0;
        }
    }

    // Print de las entradas
    printf("Nombre entrada: %s\n", nombre_archivo);
    printf("Nombre salida: %s\n", nombre_salida);
    printf("Numero filas: %i\n", numero_filas);
    printf("Numero columnas %i\n", numero_columnas);
    printf("Numero angulos %i\n", numero_angulos);
    printf("Numero distancias %i\n", numero_distancias);
    printf("umbral %f\n", umbral);

    ////////////////////////    Lectura de archivo raw         ////////////////////////
    int i;
    int j;
    int * buffer = (int*)malloc(sizeof(int));

    FILE * raw_image = fopen(nombre_archivo, "rb");
    if (raw_image == NULL)
    {
        printf("No se pudo abrir el archivo\n");
        exit(0);
    }

    // Se guarda la imagen en una matriz
    int ** matriz_image = (int**)malloc(sizeof(int*)*numero_filas);

    
    for(i=0; i<numero_filas; i++){
        matriz_image[i] = (int*)malloc(sizeof(int)*numero_columnas);
        for(j=0; j<numero_columnas; j++){
            fread(buffer, 8, 1, raw_image);
            matriz_image[i][j] = *buffer;
            //printf("%i", matriz_image[i][j]);
        }
        //printf("\n");
    }

    // Debug


    // Cáculo de ángulos
    double * angulos = (double*)malloc(sizeof(double)*numero_angulos);
    float delta_theta = 180.000/numero_angulos;
    //printf("delta theta: %f ", delta_theta);
    double theta = -90;
    for(i=0; i<numero_angulos; i++){
        angulos[i] = theta;
        //printf("angulo %i: %f ",i+1, angulos[i]);
        theta = theta + delta_theta;
    }

    // Cálculo de despazamientos
    float * desplazamientos = (float*)malloc(sizeof(float)*numero_distancias);
    float delta_ro = (numero_columnas*sqrt(2))/(2*numero_distancias);
    //printf("Delta ro: %f\n", delta_ro);
    //printf("Ro\n");
    float ro = 0;
    for(i=0; i<numero_distancias; i++){
        //printf("%f ", ro);
        desplazamientos[i] = ro;
        ro = ro + delta_ro;
    }
    //printf("\n");

    // Matriz H
    int ** H = (int**)malloc(sizeof(int*)*numero_distancias); // R 
    for(i=0; i<numero_distancias; i++){ 
        H[i] = (int*)calloc(numero_angulos, sizeof(int));     // T
        for (int j=0; j<numero_angulos; j++){
            //printf("%i ",H[i][j]);
        }
        //printf("\n");
    }
    
    // Algoritmo secuencial
    ////////////////////////    Reloj                   ////////////////////////
    clock_t start_t, end_t;
    double total_t;

    start_t = clock();
    printf("Ejecutando el arlgoritmo secuencial\n");
   
    int bordes = 0;
    int x, y, theta_i, r_j;
    for(x=0; x<numero_filas; x++){               
        for(y=0; y<numero_columnas; y++){
            if(matriz_image[x][y] == 255){
                bordes++;
                //printf("Borde: %i\n", bordes);
                for(theta_i=0; theta_i<numero_angulos; theta_i++){
                    float theta_degree = angulos[theta_i];
                    float theta_radian = angulos[theta_i]*PI/180;
                    
                    //printf("Degree:%f ", theta_degree);
                    //printf("Radian:%f ", theta_radian);
                    
                    float r1 = cos(angulos[theta_i]*PI/180);
                    float r2 = sin(angulos[theta_i]*PI/180);
                    float r_j = (x-(numero_filas/2))*r1 + (y-(numero_columnas/2))*r2;

                    //Debug
                    //printf("theta: %f ", angulos[theta_i]*PI/180);
                    //printf("rj:%f ", r_j);

                    // se sitúa en medio de la matriz y se incrementa el valor respectivo
                    int i_houghRow = (numero_distancias/2) + r_j;
                    int i_houghColum = (numero_angulos/2)+(angulos[theta_i]/delta_theta);

                    // Debug
                    //printf("r_j:%f HR:%i ",r_j, i_houghRow);
                    //printf("ang:%f HC:%i ",angulos[theta_i], i_houghColum);
                    

                    H[i_houghColum][i_houghRow] = H[i_houghColum][i_houghRow]+1;

                    //Debug
                    //printf(" - Dato guardado ");
                    //printf("\n");
                }
            }
        }
    }

    end_t = clock();
    total_t = (double)(end_t-start_t)/CLOCKS_PER_SEC;
    printf("Tiempo total del algoritmo secuencial: %f\n", total_t);

    archivo_salida = fopen(nombre_salida, "w");

    // Se escribe el archivo
    for(i=0; i<numero_angulos; i++){                   
        for (j=0; j<numero_distancias; j++){
            fwrite(&H[i][j],sizeof(int), 1, archivo_salida);
            //printf("%i ", H[i][j]);
        }
        //printf("\n");
    }
    printf("Archivo escrito \n");
    fclose(archivo_salida);

    
    printf("Algoritmo vectorial\n");
    // Algoritmo vectorial
    start_t = clock();
    printf("Ejecutando el arlgoritmo secuencial\n");

    float buffer_r_j[4] __attribute__((aligned(32))) = {0.0, 0.0, 0.0, 0.0};
    __m128 r_x,r_y, r_cos_theta, r_sin_theta, r_cos_x, r_sin_y,r_r_j;

    bordes = 0;
    if(numero_filas%4==0){
        for(x=0; x<numero_filas; x++){
            for(y=0; y<numero_columnas; y++){
                if(matriz_image[x][y] == 255){
                    r_x = _mm_set1_ps(x-(numero_filas/2));
                    r_y = _mm_set1_ps(y-(numero_columnas/2));
                    if(numero_angulos%4==0){
                        for(theta_i=0; theta_i<numero_angulos;theta_i+=4){
                            r_cos_theta = _mm_set_ps(cos(angulos[theta_i]*PI/180), cos(angulos[theta_i+1]*PI/180), cos(angulos[theta_i+2]*PI/180), cos(angulos[theta_i+3]*PI/180));
                            r_sin_theta = _mm_set_ps(sin(angulos[theta_i]*PI/180), sin(angulos[theta_i+1]*PI/180), sin(angulos[theta_i+2]*PI/180), sin(angulos[theta_i+3]*PI/180));

                            r_cos_x = _mm_mul_ps(r_cos_theta, r_x);
                            r_sin_y = _mm_mul_ps(r_sin_theta, r_y);

                            //Debug calculo
                            //printf("X: %f Y: %f Theta: %f xCos(t): %f\n", r_x[0], r_y[0], angulos[theta_i+3], r_cos_x[0]);

                            r_r_j = _mm_add_ps(r_cos_x, r_sin_y);

                            // Se guardan los datos
                            // se sitúa en medio de la matriz y se incrementa el valor respectivo
                            int i_houghRow_1 = (numero_distancias/2) + r_r_j[0];
                            int i_houghColum_1 = (numero_angulos/2)+angulos[theta_i+3]/delta_theta;

                            int i_houghRow_2 = (numero_distancias/2) + r_r_j[1];
                            int i_houghColum_2 = (numero_angulos/2)+angulos[theta_i+2]/delta_theta;

                            int i_houghRow_3 = (numero_distancias/2) + r_r_j[2];
                            int i_houghColum_3 = (numero_angulos/2)+angulos[theta_i+1]/delta_theta;

                            int i_houghRow_4 = (numero_distancias/2) + r_r_j[3];
                            int i_houghColum_4 = (numero_angulos/2)+angulos[theta_i]/delta_theta;


                            H[i_houghColum_1][i_houghRow_1] = H[i_houghColum_1][i_houghRow_1]+1;
                            H[i_houghColum_2][i_houghRow_2] = H[i_houghColum_2][i_houghRow_2]+1;
                            H[i_houghColum_3][i_houghRow_3] = H[i_houghColum_3][i_houghRow_3]+1;
                            H[i_houghColum_4][i_houghRow_4] = H[i_houghColum_4][i_houghRow_4]+1;

                            // Debug
                            //printf("r_j:%f HR:%i ",r_j, i_houghRow);
                            //printf("ang:%f HC:%i ",angulos[theta_i], i_houghColum);
                        }
                    }
                }
            }
        }
    }                

    end_t = clock();
    total_t = (double)(end_t-start_t)/CLOCKS_PER_SEC;
    printf("Tiempo total del algoritmo vectorial: %f\n", total_t);
    
    archivo_salida = fopen("hough-vectorial.raw", "w");

    // Se escribe el archivo
    for(i=0; i<numero_angulos; i++){                   
        for (j=0; j<numero_distancias; j++){
            fwrite(&H[i][j],sizeof(int), 1, archivo_salida);
            //printf("%i ", H[i][j]);
        }
        //printf("\n");
    }
    printf("Archivo escrito \n");
    fclose(archivo_salida);


    
    return 0;
}